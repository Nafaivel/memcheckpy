from enum import Enum
from typing import Union
from fastapi import FastAPI
import subprocess


class AlarmKind(Enum):
    MemoryFull = "MemoryFull"

    def alarm_reaction(self) -> str:
        match self.value:
            case self.MemoryFull.value:
                subprocess.run(
                    ["notify-send", "-u", "critical", "AlarmServer", "Memorry Full"]
                )
                return "mem full"
            case _:
                return "wrong val"


app = FastAPI()


@app.get("/")
def read_root():
    alarm = AlarmKind.MemoryFull
    return {"kind": alarm}
    return {"Hello": "World"}


@app.post("/v1/alarm")
def alarm(kind: Union[str, None] = None):
    if kind != None:
        try:
            alarm_kind = AlarmKind(kind)
            print(alarm_kind.alarm_reaction())
            return {"response": alarm_kind.value}
        except:
            return {"response": "wrong value"}

    return {"response": "add kind"}
