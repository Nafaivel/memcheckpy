from psutil import virtual_memory

# it also can be geted through $env_var_percent but it's easier to test and i dot't have requirements like this
from argparse import ArgumentParser

# there is no need to async so...
from time import sleep
import requests
import validators


def main():
    parser = ArgumentParser(
        prog="memcheckpy",
        description="gen warn based on mem usage and send to server via http api",
    )

    parser.add_argument("-u", "--url", action="store")
    parser.add_argument(
        "-l",
        "--limit",
        action="store",
        help="ram limit in persents before send warn to url",
        default=80,
    )
    parser.add_argument(
        "-t",
        "--timeout",
        action="store",
        help="timeout in seconds (check each seconds)",
        default=1,
    )

    args = parser.parse_args()

    load_limit = float(args.limit)
    timeout = float(args.timeout)
    url = args.url
    if validators.url(url) != True:
        print("wrong url")
        exit(1)

    while True:
        if virtual_memory().percent > int(load_limit):
            requests.post(url + "/v1/alarm?kind=MemoryFull")  # hardcoded? Yes
        print(virtual_memory().percent)
        sleep(timeout)


if __name__ == "__main__":
    main()
