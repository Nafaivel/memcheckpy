# Memcheckpy + server

## Depends on
- poetry
- notify-send (libnotify)

## How to use

install/update dependencies \
`poetry update`

run server \
`poetry run uvicorn srv:app`

run memcheckpy \
`poetry run python ./main.py -l 90 -u http://127.0.0.1:8000`

## Options
memcheckpy [-h] [-u URL] [-l LIMIT] [-t TIMEOUT]
- -h, --help
- -u URL, --url URL
- -l LIMIT, --limit LIMIT
  ram limit in persents before send warn to url
- -t TIMEOUT, --timeout TIMEOUT
  timeout in seconds (check each seconds)
